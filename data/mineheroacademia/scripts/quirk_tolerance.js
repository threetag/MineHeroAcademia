var maxLevel = 5;



eventManager.on("registerThreeData", function(e) {
    if (e.getEntity().isPlayer()) {
        e.register(threeDataBuilder.create("quirk_tolerance", "integer"), 0);
        e.register(threeDataBuilder.create("quirk_tolerance_level", "integer"), 1);
    }
});

// when entity is ready to join the world
eventManager.on("entityJoinWorld", function(e) {
    if (e.getEntity().isPlayer()) {
        var level = e.getEntity().getThreeData("quirk_tolerance_level");
        e.getWorld().executeCommand("/bossbar add mineheroacademia:quirk_tolerance_" + e.getEntity().getUUID() + " \"Quirk Tolerance\"");
        e.getWorld().executeCommand("/bossbar set mineheroacademia:quirk_tolerance_" + e.getEntity().getUUID() + " max " + (level * 100));
        e.getWorld().executeCommand("/bossbar set mineheroacademia:quirk_tolerance_" + e.getEntity().getUUID() + " color yellow");
        e.getWorld().executeCommand("/bossbar set mineheroacademia:quirk_tolerance_" + e.getEntity().getUUID() + " style notched_10");
        e.getWorld().executeCommand("/bossbar set mineheroacademia:quirk_tolerance_" + e.getEntity().getUUID() + " players " + e.getEntity().getName());
    }
});

// every entity tick
eventManager.on("livingUpdate", function(e) {
    // if the entity is a player AND every 100ticks/5seconds
    if (e.getEntity().isPlayer()) {
        var level = (6 - e.getEntity().getThreeData("quirk_tolerance_level")) * 10;

        if(e.getEntity().getTicksExisted() % level == 0) {
            addQuirkTolerance(e.getEntity(), 1);
        }
    }
});


function addQuirkTolerance(entity, points) {
    var currentValue = entity.getThreeData("quirk_tolerance");
    var newValue = currentValue + points;
    var max = entity.getThreeData("quirk_tolerance_level") * 100;

    if(newValue < 0) {
        newValue = 0;
    } else if(newValue > max) {
        newValue = max;
    }

    if (currentValue != newValue) {
        entity.setThreeData("quirk_tolerance", newValue);
        entity.getWorld().executeCommand("/bossbar set mineheroacademia:quirk_tolerance_" + entity.getUUID() + " value " + newValue);
    }
}
